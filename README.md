# slalom-demo-sync

# Slalom Demo – Sync data between two cloud providers

## Use Case

Source data from Google Cloud Platform BigQuery need to be synced with Oracle backend. Here are the business requirements:

1.	Transformation (json to csv)
2.	Enrichment (cross reference information need to be fetched by invoking report common service)
3.	Security (csv data should be base64encoded)
4.	Error Handling (allow manual support to modify the error data and kick off the sync process as needed)
5.	Audit reconciliation
